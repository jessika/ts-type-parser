import requests
from bs4 import BeautifulSoup

main_url = "https://microsoft.github.io/language-server-protocol/specifications/specification-current"

def fetch():
  result = requests.get(main_url)
  f = open('io/input', 'w+')
  f.write(result.text)
  f.close()

def parse():
  f = open('io/input', 'r')
  result = f.read()
  f.close()
  soup = BeautifulSoup(result, 'html.parser')
  c = [x for x in soup.findAll('div', {'class', "language-typescript"})]
  s = [x.get_text() for x in c]
  f2 = open('io/ts', 'w+')
  f2.write(''.join(s))
  f2.close()
