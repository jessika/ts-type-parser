import collections
import re
import sys
import getopt
import elixirify

Token = collections.namedtuple('Token', ['type', 'value', 'line', 'column'])

# TODO:
# enum: enum Color {Red = 1, Green, Blue}
# declare?

def main(argv):
  inputFile = None
  outputFile = None
  try:
    opts, args = getopt.getopt(argv, 'i:o:')
  except getopt.GetoptError:
    print('parse.py -i <inputfile> -o <outputfile>')
    sys.exit(2)
  for opt, arg in opts:
    if opt == '-i':
      inputFile = arg
    elif opt == '-o':
      outputFile = arg
 
  f = open(inputFile, 'r')

  if (outputFile != None):
    f2 = open(outputFile, 'w+')
    for token in tokenize(f.read()):
      f2.write(str(token) + '\n')
    f2.close()
  else:
    output = []
    for token in tokenize(f.read()):
      output.append(token)
    print(''.join(elixirify.elixirify(output)))

  f.close()

def tokenize(code):
  keywords = {'interface', 'class', 'export', 'type', 'namespace', 'extends', 'enum', 'void', 'never', 'readonly'}
  types = ['string', 'boolean', 'number', 'null', 'undefined', 'array', 'object', 'any']
  ts = '|'.join(types)
  token_specification = [
    ('CONST',       r'(?<=const )[\w]+(?=:)'),      # Constant
    ('PROP',        r'[\w?]+(?=:)'),                # Property
    ('VALUE',       r'(?<= \= ).+(?=;)'),           # Constant value
    ('OPEN',        r'{'),                          # Opening bracket
    ('CLOSE',       r'}|};'),                       # Closing bracket
    # ('UNION',       r''),   # Union type, e.g. string | number | null
    ('TUPLE_TYPE',  r'(?<=\[)(' + ts + r'), (' + ts + r')(?=\])'), # e.g. [string, number]
    ('ARRAY_TYPE',  r'(' + ts + r')(?=\[\])|(?<=Array\<)(' + ts + r')(?=\>)'), # e.g. string[] or Array<string>
    ('TYPE',        r'' + ts),                      # TS type
    ('NEWLINE',     r'\n'),                         # Line endings
    ('COMMENT',     r'(?<=((\* )|(\/\/))).+'),      # Single-line comment or multipe-line comment rows starting with *  
    ('ID',          r'[A-Za-z]+'),                  # Identifier
  ]
  tok_regex = '|'.join('(?P<%s>%s)' % pair for pair in token_specification)
  # print(tok_regex)
  line_num = 1
  line_start = 0
  for m in re.finditer(tok_regex, code):
    kind = m.lastgroup
    value = m.group()
    column = m.start() - line_start
    if kind == 'ID' and value in keywords:
      kind = value 
    elif kind == 'TUPLE_TYPE':
      value = value.split(',')
    elif kind == 'NEWLINE':
      line_start = m.end()
      line_num += 1
    yield Token(kind, value, line_num, column)

if __name__ == "__main__":
  main(sys.argv[1:])

interface = '''
export interface CompletionParams extends TextDocumentPositionParams, WorkDoneProgressParams, PartialResultParams {
	/**
	 * The completion context. This is only available if the client specifies
	 * to send this using `ClientCapabilities.textDocument.completion.contextSupport === true`
	 */
	context?: CompletionContext;
  prop1: string;
  prop2: boolean;
  prop3: string[];
  prop4: string | [number, number];
  prop5: Array<string>;
}'''

namespace = '''
export namespace ErrorCodes {
	// Defined by JSON RPC
	export const ParseError: number = -32700;
	export const InvalidRequest: number = -32600;
	export const MethodNotFound: number = -32601;
	export const InvalidParams: number = -32602;
	export const InternalError: number = -32603;
	export const serverErrorStart: number = -32099;
	export const serverErrorEnd: number = -32000;
	export const ServerNotInitialized: number = -32002;
	export const UnknownErrorCode: number = -32001;

	// Defined by the protocol.
	export const RequestCancelled: number = -32800;
	export const ContentModified: number = -32801;
}'''

print_tokens = ['ID', 'PROP', 'CONST']
# print_tokens = ['TYPE', 'ARRAY_TYPE', 'TUPLE_TYPE']

# for token in tokenize(interface):
#   # if token.type in print_tokens:
#     print(token)

# for token in tokenize(namespace):
  # if token.type in print_tokens:
    # print(token)
