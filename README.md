## ts-to-ex

Parse TypeScript type defs to elixir with python 3 (for LSP doc.)

# Files

- parse

Parse TS type def. to Tokens (named tuple)

`tokenize("ts")`

- elixirify

Read a list of Tokens and output elixir defs

`elixirify([Token])`

- scrapy

Scrapes the LSP doc. for typescript code segments. 

`fetch()`
Writes the doc. content to 'input' file.

`parse`
Parses the content from 'input' and writes to 'ts' file