import collections
import re

Token = collections.namedtuple('Token', ['type', 'value', 'line', 'column'])

type_dict = {
  'string': 'String.t()',
  'boolean': 'boolean()',
  'number': 'number()',
  'null': 'nil',
  'undefined': 'nil',
  'array': 'A',
  'object': '%{}',
  'any': 'ANY?',
}

kw_dict = {
  'interface': 'I',
  'namespace': 'N',
  'export': '',
  'type': 'T',
  'extends': 'EXT',
  'enum': 'E',
}

keywords = {'interface', 'namespace', 'export', 'type', 'extends', 'enum'}

def elixirify(tokens):
  # types = ['string', 'boolean', 'number', 'null', 'undefined', 'array', 'object', 'any']
  output = []

  for i, t in enumerate(tokens):    
    value = parse(t)
    if t.type in keywords:
      value = kw_dict[t.value]
      if t.type =='interface':
        end = get_end(i, tokens)
        value = interface(tokens[i:i + end])
        output.append(value)
        i = i + end
      elif t.type == 'namespace':
        end = get_end(i, tokens)
        value = namespace(tokens[i:i + end])
        output.append(value)
        i = i + end
    output.append(value)

  return output

def parse(t):
    value = t.value
    if t.type == 'PROP':
      if is_optional(value): # optional props: [name]?
        value = strip_optional(value) + ' (OPTIONAL)'
      value = value + ':'
    elif t.type == 'TYPE':
      value = type_dict[t.value]
    elif t.type == 'OPEN':
      value = 'do'
    elif t.type == 'CLOSE':
      value = 'end'
    elif t.type == 'ARRAY_TYPE':
      value = type_dict[t.value] + ' []'
    elif t.type == 'TUPLE_TYPE':
      value = '{' + ', '.join(type_dict[v.strip()] for v in t.value)  + '}'
    elif t.type == 'COMMENT':
      value = "# " + value
    # elif t.type == 'NEWLINE':
    #   continue
    return value

prop_types = ['ID', 'TYPE', 'TUPLE_TYPE', 'ARRAY_TYPE']

# interface ->
# defmodule Name do
#   @enforce_keys [:prop1]
#   defstruct [:prop1, prop2: "default_value"]
# 
#   @type t() :: %__MODULE__{
#      prop1: String.t(),
#      prop2: String.t() | number(),
#      prop3: $NameofOtherStruct{}
# }
# end

def interface(tokens):
  extensions = extends(tokens)
  props = [] 
  # parse all props (optional, array, tuple, basic type)
  for p in get_in('type', 'PROP', tokens):
    attrs = list(filter(lambda x: x.line == p.line and x.type in prop_types, tokens)) # get Tokens on the same line
    det = [struct(x) if x.type == 'ID' else parse(x) for x in attrs] if len(attrs) else '' # Get all related types
    props.append([p.value, det])

  # Optional vs. required params
  prop_names = [':' + x[0] for x in props]
  required_props = list(filter(lambda x: not is_optional(x), prop_names))
  prop_names_c = [strip_optional(x) for x in prop_names]

  return ' '.join([
    'defmodule', tokens[1].value, 'do', '\n\n',
    '\t', '@enforce_kyes', '[', ', '.join(required_props), ']' '\n',
    '\t', 'defstruct', '[', ', '.join(prop_names_c), ']', '\n\n',
    '\t', '@type', 't()', '::', '%__MODULE__{', '\n',
    ',\n'.join(types(props)), '\n',
    ',\n'.join(types(extensions)), '\n',
    '\t', '}', '\n',
    'end', '\n',
  ])

def struct(t):
  return '%' + t.value + '{}'

def types(props):
  return [''.join([
    '\t\t',
    strip_optional(p[0]),
    ': ',
    ' | '.join(p[1]),
  ]) for p in props]

def extends(tokens):
  p = tokens[0]
  first_line = list(filter(lambda x: x.line == p.line, tokens))
  index = [i for i, x in enumerate(first_line) if x.type == 'extends']
  if (len(index) > 0):
    interfaces = first_line[index[0] + 1: len(first_line) - 1]
    extensions = [['_' + x.value, ['%' + x.value + '{}']] for x in interfaces]
    return extensions
  return []

# namespace ->
# defmodule Name do
#   @constName1 value
#   def constName1, do: @constName1
# end

def namespace(tokens):
  consts = []
  for c in get_in('type', 'CONST', tokens):
    attrs = list(filter(lambda x: x.line == c.line and x.type == 'VALUE', tokens)) # get Tokens on the same line
    consts.append([c.value, attrs[0].value])

  return ' '.join([
    'defmodule', tokens[1].value, 'do', '\n',
    '\n'.join([constant(c) for c in consts]),
    'end', '\n',
  ])

# Add type
def constant(c):
  return ' '.join([
    '\t', '@' + c[0], c[1], '\n',
    '\t', 'def', c[0], 'do:', '@' + c[0], '\n',
  ])

# ======== Help Functions =========

def strip_optional(val):
  return val[:len(val) -1] if is_optional(val) else val

def is_optional(val):
  return re.search(r'\?', val)

def get_in(key, val, l):
 return list(filter(lambda x: getattr(x, key) == val, l))

# Get index of corresonding Close Token
def get_end(i, tokens):
  return [x.type == 'CLOSE' for x in tokens[i:]].index(True)


# extends interface2 -> ?
# 

i_interface = [
  Token(type='NEWLINE', value='\n', line=2, column=0),
  Token(type='export', value='export', line=2, column=0),
  Token(type='interface', value='interface', line=2, column=7),
  Token(type='ID', value='CompletionParams', line=2, column=17),
  Token(type='extends', value='extends', line=2, column=34),
  Token(type='ID', value='TextDocumentPositionParams', line=2, column=42),
  Token(type='ID', value='WorkDoneProgressParams', line=2, column=70),
  Token(type='ID', value='PartialResultParams', line=2, column=94),
  Token(type='OPEN', value='{', line=2, column=114),
  Token(type='NEWLINE', value='\n', line=3, column=115),
  Token(type='NEWLINE', value='\n', line=4, column=4),
  Token(type='COMMENT', value='The completion context. This is only available if the client specifies', line=4, column=4),
  Token(type='NEWLINE', value='\n', line=5, column=74),
  Token(type='COMMENT', value='to send this using `ClientCapabilities.textDocument.completion.contextSupport === true`', line=5, column=4),
  Token(type='NEWLINE', value='\n', line=6, column=91),
  Token(type='NEWLINE', value='\n', line=7, column=4),
  Token(type='PROP', value='context?', line=7, column=1),
  Token(type='ID', value='CompletionContext', line=7, column=11),
  Token(type='NEWLINE', value='\n', line=8, column=29),
  Token(type='PROP', value='prop1', line=8, column=2),
  Token(type='TYPE', value='string', line=8, column=9),
  Token(type='NEWLINE', value='\n', line=9, column=16),
  Token(type='PROP', value='prop2', line=9, column=2),
  Token(type='TYPE', value='boolean', line=9, column=9),
  Token(type='NEWLINE', value='\n', line=10, column=17),
  Token(type='PROP', value='prop3', line=10, column=2),
  Token(type='ARRAY_TYPE', value='string', line=10, column=9),
  Token(type='NEWLINE', value='\n', line=11, column=18),
  Token(type='PROP', value='prop4', line=11, column=2),
  Token(type='TYPE', value='string', line=11, column=9),
  Token(type='TUPLE_TYPE', value=['number', ' number'], line=11, column=19),
  Token(type='NEWLINE', value='\n', line=12, column=34),
  Token(type='CLOSE', value='}', line=12, column=0),
  Token(type='NEWLINE', value='\n', line=13, column=1),
]

i_namespace = [
  Token(type='NEWLINE', value='\n', line=2, column=0) ,
  Token(type='export', value='export', line=2, column=0) ,
  Token(type='namespace', value='namespace', line=2, column=7) ,
  Token(type='ID', value='ErrorCodes', line=2, column=17) ,
  Token(type='OPEN', value='{', line=2, column=28) ,
  Token(type='NEWLINE', value='\n', line=3, column=29) ,
  Token(type='COMMENT', value=' Defined by JSON RPC', line=3, column=3) ,
  Token(type='NEWLINE', value='\n', line=4, column=23) ,
  Token(type='export', value='export', line=4, column=1) ,
  Token(type='const', value='const', line=4, column=8) ,
  Token(type='CONST', value='ParseError', line=4, column=14) ,
  Token(type='TYPE', value='number', line=4, column=26) ,
  Token(type='VALUE', value='-32700', line=4, column=35) ,
  Token(type='NEWLINE', value='\n', line=5, column=42) ,
  Token(type='export', value='export', line=5, column=1) ,
  Token(type='const', value='const', line=5, column=8) ,
  Token(type='CONST', value='InvalidRequest', line=5, column=14) ,
  Token(type='TYPE', value='number', line=5, column=30) ,
  Token(type='VALUE', value='-32600', line=5, column=39) ,
  Token(type='NEWLINE', value='\n', line=6, column=46) ,
  Token(type='export', value='export', line=6, column=1) ,
  Token(type='const', value='const', line=6, column=8) ,
  Token(type='CONST', value='MethodNotFound', line=6, column=14) ,
  Token(type='TYPE', value='number', line=6, column=30) ,
  Token(type='VALUE', value='-32601', line=6, column=39) ,
  Token(type='NEWLINE', value='\n', line=7, column=46) ,
  Token(type='export', value='export', line=7, column=1) ,
  Token(type='const', value='const', line=7, column=8) ,
  Token(type='CONST', value='InvalidParams', line=7, column=14) ,
  Token(type='TYPE', value='number', line=7, column=29) ,
  Token(type='VALUE', value='-32602', line=7, column=38) ,
  Token(type='NEWLINE', value='\n', line=8, column=45) ,
  Token(type='export', value='export', line=8, column=1) ,
  Token(type='const', value='const', line=8, column=8) ,
  Token(type='CONST', value='InternalError', line=8, column=14) ,
  Token(type='TYPE', value='number', line=8, column=29) ,
  Token(type='VALUE', value='-32603', line=8, column=38) ,
  Token(type='NEWLINE', value='\n', line=9, column=45) ,
  Token(type='export', value='export', line=9, column=1) ,
  Token(type='const', value='const', line=9, column=8) ,
  Token(type='CONST', value='serverErrorStart', line=9, column=14) ,
  Token(type='TYPE', value='number', line=9, column=32) ,
  Token(type='VALUE', value='-32099', line=9, column=41) ,
  Token(type='NEWLINE', value='\n', line=10, column=48) ,
  Token(type='export', value='export', line=10, column=1) ,
  Token(type='const', value='const', line=10, column=8) ,
  Token(type='CONST', value='serverErrorEnd', line=10, column=14) ,
  Token(type='TYPE', value='number', line=10, column=30) ,
  Token(type='VALUE', value='-32000', line=10, column=39) ,
  Token(type='NEWLINE', value='\n', line=11, column=46) ,
  Token(type='export', value='export', line=11, column=1) ,
  Token(type='const', value='const', line=11, column=8) ,
  Token(type='CONST', value='ServerNotInitialized', line=11, column=14) ,
  Token(type='TYPE', value='number', line=11, column=36) ,
  Token(type='VALUE', value='-32002', line=11, column=45) ,
  Token(type='NEWLINE', value='\n', line=12, column=52) ,
  Token(type='export', value='export', line=12, column=1) ,
  Token(type='const', value='const', line=12, column=8) ,
  Token(type='CONST', value='UnknownErrorCode', line=12, column=14) ,
  Token(type='TYPE', value='number', line=12, column=32) ,
  Token(type='VALUE', value='-32001', line=12, column=41) ,
  Token(type='NEWLINE', value='\n', line=13, column=48) ,
  Token(type='NEWLINE', value='\n', line=14, column=0) ,
  Token(type='COMMENT', value=' Defined by the protocol.', line=14, column=3) ,
  Token(type='NEWLINE', value='\n', line=15, column=28) ,
  Token(type='export', value='export', line=15, column=1) ,
  Token(type='const', value='const', line=15, column=8) ,
  Token(type='CONST', value='RequestCancelled', line=15, column=14) ,
  Token(type='TYPE', value='number', line=15, column=32) ,
  Token(type='VALUE', value='-32800', line=15, column=41) ,
  Token(type='NEWLINE', value='\n', line=16, column=48) ,
  Token(type='export', value='export', line=16, column=1) ,
  Token(type='const', value='const', line=16, column=8) ,
  Token(type='CONST', value='ContentModified', line=16, column=14) ,
  Token(type='TYPE', value='number', line=16, column=31) ,
  Token(type='VALUE', value='-32801', line=16, column=40) ,
  Token(type='NEWLINE', value='\n', line=17, column=47) ,
  Token(type='CLOSE', value='}', line=17, column=0) ,
]

# print(' '.join(elixirify(i_interface)))
# print(' '.join(elixirify(i_namespace)))
